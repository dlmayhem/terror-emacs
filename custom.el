;;; custom.el -*- lexical-binding: t; -*-

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-file-apps
   '((auto-mode . emacs)
     (directory . emacs)
     ("\\.mm\\'" . default)
     ("\\.odt\\'" . "lowriter %s")
     ("\\.x?html?\\'" . "qutebrowser %s")
     ("\\.pdf\\'" . emacs)))
 '(package-selected-packages
   '(emacsql org-remark embark-consult consult pyenv-mode blacken python-black pyvenv-auto pyvenv modus-themes ef-themes evangelion-theme ace-window extempore-mode prettier eglot impatient-mode emmet-mode web-mode spacious-padding gnu-elpa-keyring-update minions tree-sitter-langs tree-sitter dired-preview telega autothemer terror-wilmersdorf nerd-icons-completion nerd-icons-dired geiser-guile fetch oc logos vertico-posframe el-fetch ob-python org-noter-pdftools citar-org-roam citar-org org-cite ebib-biblio ebib dired-sidebar org-contrib org-plus-contrib ox-extra citar-embark org-roam-export org-roam-ui org-roam-bibtex hide-mode-line htmlize simple-httpd org-modern corfu-terminal corfu lilypond-mode LilyPond-mode typing rainbow-mode arxiv-mode nov writeroom-mode hide-lines smartparens yasnippet pdf-loader elfeed-org org-bullets ox-bibtex embark elfeed org-ref citar org-tree-slide visual-fill-column dired-open dired-hide-dotfiles auctex markdown-mode org-present ob-latex-as-png ox-twbs org-roam pdf-view-restore lorem-ipsum doom-themes solaire-mode rainbow-delimiters magit which-key orderless marginalia vertico use-package pdf-tools))
 '(send-mail-function 'mailclient-send-it)
 '(warning-suppress-types
   '((server)
     (server)
     (server)
     (server)
     (org-element-cache)
     (org-element-cache)
     (org-element-cache)
     ((savehist-file))
     (use-package)
     (comp))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
