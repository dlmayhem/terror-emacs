;; tomorrow-night-theme.el --- Tomorrow Night color theme

;; Copyright 2015-2019 Jonathan Chu

;; Author: Javier Pacheco <jpacheco@cock.li>
;; Version: 0.0.1

;; This file is not part of GNU Emacs.

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This file is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; An Emacs port of the Tomorrow Night theme.

;;; Code:

(deftheme tomorrow-night
  "Tomorrow Night - An Emacs port of the Tomorrow Night theme.")

(defvar tomorrow-night-colors-alist
  (let* ((256color  (eq (display-color-cells (selected-frame)) 256))
         (colors `(("tomorrow-night-accent"   . "#41728e")
                   ("tomorrow-night-fg"       . (if ,256color "color-248" "#c5c8c6"))
                   ("tomorrow-night-bg"       . (if ,256color "color-235" "#161719"))
                   ("tomorrow-night-bg-1"     . (if ,256color "color-234" "#0d0d0d"))
                   ("tomorrow-night-bg-hl"    . (if ,256color "color-236" "#1b1b1b"))
                   ("tomorrow-night-gutter"   . (if ,256color "color-239" "#212122"))
                   ("tomorrow-night-mono-1"   . (if ,256color "color-248" "#292b2b"))
                   ("tomorrow-night-mono-2"   . (if ,256color "color-244" "#3f4040"))
                   ("tomorrow-night-mono-3"   . (if ,256color "color-240" "#5c5e5e"))
                   ("tomorrow-night-cyan"     . "#8abeb7")				   
                   ("tomorrow-night-blue"     . "#81a2be")
                   ("tomorrow-night-purple"   . "#b294bb")
                   ("tomorrow-night-green"    . "#b5bd68")
                   ("tomorrow-night-red-1"    . "#cc6666")
                   ("tomorrow-night-red-2"    . "#cc6666")
                   ("tomorrow-night-orange-1" . "#de935f")
                   ("tomorrow-night-orange-2" . "#de935f")
                   ("tomorrow-night-gray"     . (if ,256color "color-237" "#5a5b5a"))
                   ("tomorrow-night-silver"   . (if ,256color "color-247" "#9DA5B4"))
                   ("tomorrow-night-black"    . (if ,256color "color-233" "#21252B"))
                   ("tomorrow-night-border"   . (if ,256color "color-232" "#181A1F")))))
    colors)
  "List of Tomorrow Night colors.")

(defmacro tomorrow-night-with-color-variables (&rest body)
  "Bind the colors list around BODY."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@ (mapcar (lambda (cons)
                      (list (intern (car cons)) (cdr cons)))
                    tomorrow-night-colors-alist))
     ,@body))

(tomorrow-night-with-color-variables
  (custom-theme-set-faces
   'tomorrow-night

   `(default ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg))))
   `(success ((t (:foreground ,tomorrow-night-green))))
   `(warning ((t (:foreground ,tomorrow-night-orange-2))))
   `(error ((t (:foreground ,tomorrow-night-red-1 :weight bold))))
   `(link ((t (:foreground ,tomorrow-night-blue :underline t :weight bold))))
   `(link-visited ((t (:foreground ,tomorrow-night-blue :underline t :weight normal))))
   `(cursor ((t (:background ,tomorrow-night-accent))))
   `(fringe ((t (:background ,tomorrow-night-bg))))
   `(region ((t (:background ,tomorrow-night-gray :distant-foreground ,tomorrow-night-mono-2))))
   `(highlight ((t (:background ,tomorrow-night-gray :distant-foreground ,tomorrow-night-mono-2))))
   `(hl-line ((t (:background ,tomorrow-night-bg-hl :distant-foreground nil))))
   `(header-line ((t (:background ,tomorrow-night-black))))
   `(vertical-border ((t (:background ,tomorrow-night-border :foreground ,tomorrow-night-border))))
   `(secondary-selection ((t (:background ,tomorrow-night-bg-1))))
   `(query-replace ((t (:inherit (isearch)))))
   `(minibuffer-prompt ((t (:foreground ,tomorrow-night-silver))))
   `(tooltip ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg-1 :inherit variable-pitch))))

   `(font-lock-builtin-face ((t (:foreground ,tomorrow-night-cyan))))
   `(font-lock-comment-face ((t (:foreground ,tomorrow-night-mono-3 :slant italic))))
   `(font-lock-comment-delimiter-face ((default (:inherit (font-lock-comment-face)))))
   `(font-lock-doc-face ((t (:inherit (font-lock-string-face)))))
   `(font-lock-function-name-face ((t (:foreground ,tomorrow-night-blue))))
   `(font-lock-keyword-face ((t (:foreground ,tomorrow-night-purple :weight normal))))
   `(font-lock-preprocessor-face ((t (:foreground ,tomorrow-night-mono-2))))
   `(font-lock-string-face ((t (:foreground ,tomorrow-night-green))))
   `(font-lock-type-face ((t (:foreground ,tomorrow-night-orange-2))))
   `(font-lock-constant-face ((t (:foreground ,tomorrow-night-cyan))))
   `(font-lock-variable-name-face ((t (:foreground ,tomorrow-night-red-1))))
   `(font-lock-warning-face ((t (:foreground ,tomorrow-night-mono-3 :bold t))))
   `(font-lock-negation-char-face ((t (:foreground ,tomorrow-night-cyan :bold t))))

   ;; mode-line
   `(mode-line ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-silver :box (:color ,tomorrow-night-border :line-width 2)))))
   `(mode-line-buffer-id ((t (:weight bold))))
   `(mode-line-emphasis ((t (:weight bold))))
   `(mode-line-inactive ((t (:background ,tomorrow-night-border :foreground ,tomorrow-night-gray :box (:color ,tomorrow-night-border :line-width 1)))))

   ;; window-divider
   `(window-divider ((t (:foreground ,tomorrow-night-border))))
   `(window-divider-first-pixel ((t (:foreground ,tomorrow-night-border))))
   `(window-divider-last-pixel ((t (:foreground ,tomorrow-night-border))))

   ;; custom
   `(custom-state ((t (:foreground ,tomorrow-night-green))))

   ;; ido
   `(ido-first-match ((t (:foreground ,tomorrow-night-purple :weight bold))))
   `(ido-only-match ((t (:foreground ,tomorrow-night-red-1 :weight bold))))
   `(ido-subdir ((t (:foreground ,tomorrow-night-blue))))
   `(ido-virtual ((t (:foreground ,tomorrow-night-mono-3))))

   ;; ace-jump
   `(ace-jump-face-background ((t (:foreground ,tomorrow-night-mono-3 :background ,tomorrow-night-bg-1 :inverse-video nil))))
   `(ace-jump-face-foreground ((t (:foreground ,tomorrow-night-red-1 :background ,tomorrow-night-bg-1 :inverse-video nil))))

   ;; ace-window
   `(aw-background-face ((t (:inherit font-lock-comment-face))))
   `(aw-leading-char-face ((t (:foreground ,tomorrow-night-red-1 :weight bold))))

   ;; centaur-tabs
   `(centaur-tabs-default ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-black))))
   `(centaur-tabs-selected ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-fg :weight bold))))
   `(centaur-tabs-unselected ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-fg :weight light))))
   `(centaur-tabs-selected-modified ((t (:background ,tomorrow-night-bg
                                                     :foreground ,tomorrow-night-blue :weight bold))))
   `(centaur-tabs-unselected-modified ((t (:background ,tomorrow-night-black :weight light
                                                       :foreground ,tomorrow-night-blue))))
   `(centaur-tabs-active-bar-face ((t (:background ,tomorrow-night-accent))))
   `(centaur-tabs-modified-marker-selected ((t (:inherit 'centaur-tabs-selected :foreground,tomorrow-night-accent))))
   `(centaur-tabs-modified-marker-unselected ((t (:inherit 'centaur-tabs-unselected :foreground,tomorrow-night-accent))))

   ;; company-mode
   `(company-tooltip ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg-1))))
   `(company-tooltip-annotation ((t (:foreground ,tomorrow-night-mono-2 :background ,tomorrow-night-bg-1))))
   `(company-tooltip-annotation-selection ((t (:foreground ,tomorrow-night-mono-2 :background ,tomorrow-night-gray))))
   `(company-tooltip-selection ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-gray))))
   `(company-tooltip-mouse ((t (:background ,tomorrow-night-gray))))
   `(company-tooltip-common ((t (:foreground ,tomorrow-night-orange-2 :background ,tomorrow-night-bg-1))))
   `(company-tooltip-common-selection ((t (:foreground ,tomorrow-night-orange-2 :background ,tomorrow-night-gray))))
   `(company-preview ((t (:background ,tomorrow-night-bg))))
   `(company-preview-common ((t (:foreground ,tomorrow-night-orange-2 :background ,tomorrow-night-bg))))
   `(company-scrollbar-fg ((t (:background ,tomorrow-night-mono-1))))
   `(company-scrollbar-bg ((t (:background ,tomorrow-night-bg-1))))
   `(company-template-field ((t (:inherit highlight))))

   ;; doom-modeline
   `(doom-modeline-bar ((t (:background ,tomorrow-night-accent))))

   ;; flyspell
   `(flyspell-duplicate ((t (:underline (:color ,tomorrow-night-orange-1 :style wave)))))
   `(flyspell-incorrect ((t (:underline (:color ,tomorrow-night-red-1 :style wave)))))

   ;; flymake
   `(flymake-error ((t (:underline (:color ,tomorrow-night-red-1 :style wave)))))
   `(flymake-note ((t (:underline (:color ,tomorrow-night-green :style wave)))))
   `(flymake-warning ((t (:underline (:color ,tomorrow-night-orange-1 :style wave)))))

   ;; flycheck
   `(flycheck-error ((t (:underline (:color ,tomorrow-night-red-1 :style wave)))))
   `(flycheck-info ((t (:underline (:color ,tomorrow-night-green :style wave)))))
   `(flycheck-warning ((t (:underline (:color ,tomorrow-night-orange-1 :style wave)))))

   ;; compilation
   `(compilation-face ((t (:foreground ,tomorrow-night-fg))))
   `(compilation-line-number ((t (:foreground ,tomorrow-night-mono-2))))
   `(compilation-column-number ((t (:foreground ,tomorrow-night-mono-2))))
   `(compilation-mode-line-exit ((t (:inherit compilation-info :weight bold))))
   `(compilation-mode-line-fail ((t (:inherit compilation-error :weight bold))))

   ;; isearch
   `(isearch ((t (:foreground ,tomorrow-night-bg :background ,tomorrow-night-purple))))
   `(isearch-fail ((t (:foreground ,tomorrow-night-red-2 :background nil))))
   `(lazy-highlight ((t (:foreground ,tomorrow-night-purple :background ,tomorrow-night-bg-1 :underline ,tomorrow-night-purple))))

   ;; diff-hl (https://github.com/dgutov/diff-hl)
   '(diff-hl-change ((t (:foreground "#E9C062" :background "#8b733a"))))
   '(diff-hl-delete ((t (:foreground "#CC6666" :background "#7a3d3d"))))
   '(diff-hl-insert ((t (:foreground "#A8FF60" :background "#547f30"))))

   ;; dired-mode
   '(dired-directory ((t (:inherit (font-lock-keyword-face)))))
   '(dired-flagged ((t (:inherit (diff-hl-delete)))))
   '(dired-symlink ((t (:foreground "#FD5FF1"))))

   ;; dired-async
   `(dired-async-failures ((t (:inherit error))))
   `(dired-async-message ((t (:inherit success))))
   `(dired-async-mode-message ((t (:foreground ,tomorrow-night-orange-1))))

   ;; helm
   `(helm-header ((t (:foreground ,tomorrow-night-mono-2
                                  :background ,tomorrow-night-bg
                                  :underline nil
                                  :box (:line-width 6 :color ,tomorrow-night-bg)))))
   `(helm-source-header ((t (:foreground ,tomorrow-night-orange-2
                                         :background ,tomorrow-night-bg
                                         :underline nil
                                         :weight bold
                                         :box (:line-width 6 :color ,tomorrow-night-bg)))))
   `(helm-selection ((t (:background ,tomorrow-night-gray))))
   `(helm-selection-line ((t (:background ,tomorrow-night-gray))))
   `(helm-visible-mark ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-orange-2))))
   `(helm-candidate-number ((t (:foreground ,tomorrow-night-green :background ,tomorrow-night-bg-1))))
   `(helm-separator ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-red-1))))
   `(helm-M-x-key ((t (:foreground ,tomorrow-night-orange-1))))
   `(helm-bookmark-addressbook ((t (:foreground ,tomorrow-night-orange-1))))
   `(helm-bookmark-directory ((t (:foreground nil :background nil :inherit helm-ff-directory))))
   `(helm-bookmark-file ((t (:foreground nil :background nil :inherit helm-ff-file))))
   `(helm-bookmark-gnus ((t (:foreground ,tomorrow-night-purple))))
   `(helm-bookmark-info ((t (:foreground ,tomorrow-night-green))))
   `(helm-bookmark-man ((t (:foreground ,tomorrow-night-orange-2))))
   `(helm-bookmark-w3m ((t (:foreground ,tomorrow-night-purple))))
   `(helm-match ((t (:foreground ,tomorrow-night-orange-2))))
   `(helm-ff-directory ((t (:foreground ,tomorrow-night-cyan :background ,tomorrow-night-bg :weight bold))))
   `(helm-ff-file ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg :weight normal))))
   `(helm-ff-executable ((t (:foreground ,tomorrow-night-green :background ,tomorrow-night-bg :weight normal))))
   `(helm-ff-invalid-symlink ((t (:foreground ,tomorrow-night-red-1 :background ,tomorrow-night-bg :weight bold))))
   `(helm-ff-symlink ((t (:foreground ,tomorrow-night-orange-2 :background ,tomorrow-night-bg :weight bold))))
   `(helm-ff-prefix ((t (:foreground ,tomorrow-night-bg :background ,tomorrow-night-orange-2 :weight normal))))
   `(helm-buffer-not-saved ((t (:foreground ,tomorrow-night-red-1))))
   `(helm-buffer-process ((t (:foreground ,tomorrow-night-mono-2))))
   `(helm-buffer-saved-out ((t (:foreground ,tomorrow-night-fg))))
   `(helm-buffer-size ((t (:foreground ,tomorrow-night-mono-2))))
   `(helm-buffer-directory ((t (:foreground ,tomorrow-night-purple))))
   `(helm-grep-cmd-line ((t (:foreground ,tomorrow-night-cyan))))
   `(helm-grep-file ((t (:foreground ,tomorrow-night-fg))))
   `(helm-grep-finish ((t (:foreground ,tomorrow-night-green))))
   `(helm-grep-lineno ((t (:foreground ,tomorrow-night-mono-2))))
   `(helm-grep-finish ((t (:foreground ,tomorrow-night-red-1))))
   `(helm-grep-match ((t (:foreground nil :background nil :inherit helm-match))))
   `(helm-swoop-target-line-block-face ((t (:background ,tomorrow-night-mono-3 :foreground "#222222"))))
   `(helm-swoop-target-line-face ((t (:background ,tomorrow-night-mono-3 :foreground "#222222"))))
   `(helm-swoop-target-word-face ((t (:background ,tomorrow-night-purple :foreground "#ffffff"))))
   `(helm-locate-finish ((t (:foreground ,tomorrow-night-green))))
   `(info-menu-star ((t (:foreground ,tomorrow-night-red-1))))

   ;; ivy
   `(ivy-confirm-face ((t (:inherit minibuffer-prompt :foreground ,tomorrow-night-green))))
   `(ivy-current-match ((t (:background ,tomorrow-night-gray :weight normal))))
   `(ivy-highlight-face ((t (:inherit font-lock-builtin-face))))
   `(ivy-match-required-face ((t (:inherit minibuffer-prompt :foreground ,tomorrow-night-red-1))))
   `(ivy-minibuffer-match-face-1 ((t (:background ,tomorrow-night-bg-hl))))
   `(ivy-minibuffer-match-face-2 ((t (:inherit ivy-minibuffer-match-face-1 :background ,tomorrow-night-black :foreground ,tomorrow-night-purple :weight semi-bold))))
   `(ivy-minibuffer-match-face-3 ((t (:inherit ivy-minibuffer-match-face-2 :background ,tomorrow-night-black :foreground ,tomorrow-night-green :weight semi-bold))))
   `(ivy-minibuffer-match-face-4 ((t (:inherit ivy-minibuffer-match-face-2 :background ,tomorrow-night-black :foreground ,tomorrow-night-orange-2 :weight semi-bold))))
   `(ivy-minibuffer-match-highlight ((t (:inherit ivy-current-match))))
   `(ivy-modified-buffer ((t (:inherit default :foreground ,tomorrow-night-orange-1))))
   `(ivy-virtual ((t (:inherit font-lock-builtin-face :slant italic))))

   ;; counsel
   `(counsel-key-binding ((t (:foreground ,tomorrow-night-orange-2 :weight bold))))

   ;; swiper
   `(swiper-match-face-1 ((t (:inherit ivy-minibuffer-match-face-1))))
   `(swiper-match-face-2 ((t (:inherit ivy-minibuffer-match-face-2))))
   `(swiper-match-face-3 ((t (:inherit ivy-minibuffer-match-face-3))))
   `(swiper-match-face-4 ((t (:inherit ivy-minibuffer-match-face-4))))

   ;; git-commit
   `(git-commit-comment-action  ((t (:foreground ,tomorrow-night-green :weight bold))))
   `(git-commit-comment-branch  ((t (:foreground ,tomorrow-night-blue :weight bold))))
   `(git-commit-comment-heading ((t (:foreground ,tomorrow-night-orange-2 :weight bold))))

   ;; git-gutter
   `(git-gutter:added ((t (:foreground ,tomorrow-night-green :weight bold))))
   `(git-gutter:deleted ((t (:foreground ,tomorrow-night-red-1 :weight bold))))
   `(git-gutter:modified ((t (:foreground ,tomorrow-night-orange-1 :weight bold))))

   ;; eshell
   `(eshell-ls-archive ((t (:foreground ,tomorrow-night-purple :weight bold))))
   `(eshell-ls-backup ((t (:foreground ,tomorrow-night-orange-2))))
   `(eshell-ls-clutter ((t (:foreground ,tomorrow-night-red-2 :weight bold))))
   `(eshell-ls-directory ((t (:foreground ,tomorrow-night-blue :weight bold))))
   `(eshell-ls-executable ((t (:foreground ,tomorrow-night-green :weight bold))))
   `(eshell-ls-missing ((t (:foreground ,tomorrow-night-red-1 :weight bold))))
   `(eshell-ls-product ((t (:foreground ,tomorrow-night-orange-2))))
   `(eshell-ls-special ((t (:foreground "#FD5FF1" :weight bold))))
   `(eshell-ls-symlink ((t (:foreground ,tomorrow-night-cyan :weight bold))))
   `(eshell-ls-unreadable ((t (:foreground ,tomorrow-night-mono-1))))
   `(eshell-prompt ((t (:inherit minibuffer-prompt))))

   ;; man
   `(Man-overstrike ((t (:inherit font-lock-type-face :weight bold))))
   `(Man-underline ((t (:inherit font-lock-keyword-face :slant italic :weight bold))))

   ;; woman
   `(woman-bold ((t (:inherit font-lock-type-face :weight bold))))
   `(woman-italic ((t (:inherit font-lock-keyword-face :slant italic :weight bold))))

   ;; dictionary
   `(dictionary-button-face ((t (:inherit widget-button))))
   `(dictionary-reference-face ((t (:inherit font-lock-type-face :weight bold))))
   `(dictionary-word-entry-face ((t (:inherit font-lock-keyword-face :slant italic :weight bold))))

   ;; erc
   `(erc-error-face ((t (:inherit error))))
   `(erc-input-face ((t (:inherit shadow))))
   `(erc-my-nick-face ((t (:foreground ,tomorrow-night-accent))))
   `(erc-notice-face ((t (:inherit font-lock-comment-face))))
   `(erc-timestamp-face ((t (:foreground ,tomorrow-night-green :weight bold))))

   ;; jabber
   `(jabber-roster-user-online ((t (:foreground ,tomorrow-night-green))))
   `(jabber-roster-user-away ((t (:foreground ,tomorrow-night-red-1))))
   `(jabber-roster-user-xa ((t (:foreground ,tomorrow-night-red-2))))
   `(jabber-roster-user-dnd ((t (:foreground ,tomorrow-night-purple))))
   `(jabber-roster-user-chatty ((t (:foreground ,tomorrow-night-orange-2))))
   `(jabber-roster-user-error ((t (:foreground ,tomorrow-night-red-1 :bold t))))
   `(jabber-roster-user-offline ((t (:foreground ,tomorrow-night-mono-3))))
   `(jabber-chat-prompt-local ((t (:foreground ,tomorrow-night-blue))))
   `(jabber-chat-prompt-foreign ((t (:foreground ,tomorrow-night-orange-2))))
   `(jabber-chat-prompt-system ((t (:foreground ,tomorrow-night-mono-3))))
   `(jabber-chat-error ((t (:inherit jabber-roster-user-error))))
   `(jabber-rare-time-face ((t (:foreground ,tomorrow-night-cyan))))
   `(jabber-activity-face ((t (:inherit jabber-chat-prompt-foreign))))
   `(jabber-activity-personal-face ((t (:inherit jabber-chat-prompt-local))))

   ;; eww
   `(eww-form-checkbox ((t (:inherit eww-form-submit))))
   `(eww-form-file ((t (:inherit eww-form-submit))))
   `(eww-form-select ((t (:inherit eww-form-submit))))
   `(eww-form-submit ((t (:background ,tomorrow-night-gray :foreground ,tomorrow-night-fg :box (:line-width 2 :color ,tomorrow-night-border :style released-button)))))
   `(eww-form-text ((t (:inherit widget-field :box (:line-width 1 :color ,tomorrow-night-border)))))
   `(eww-form-textarea ((t (:inherit eww-form-text))))
   `(eww-invalid-certificate ((t (:foreground ,tomorrow-night-red-1))))
   `(eww-valid-certificate ((t (:foreground ,tomorrow-night-green))))

   ;; js2-mode
   `(js2-error ((t (:underline (:color ,tomorrow-night-red-1 :style wave)))))
   `(js2-external-variable ((t (:foreground ,tomorrow-night-cyan))))
   `(js2-warning ((t (:underline (:color ,tomorrow-night-orange-1 :style wave)))))
   `(js2-function-call ((t (:inherit (font-lock-function-name-face)))))
   `(js2-function-param ((t (:foreground ,tomorrow-night-mono-1))))
   `(js2-jsdoc-tag ((t (:foreground ,tomorrow-night-purple))))
   `(js2-jsdoc-type ((t (:foreground ,tomorrow-night-orange-2))))
   `(js2-jsdoc-value((t (:foreground ,tomorrow-night-red-1))))
   `(js2-object-property ((t (:foreground ,tomorrow-night-red-1))))

   ;; vc-mode
   `(vc-diff-added ((t (:foreground ,tomorrow-night-green))))
   `(vc-diff-removed ((t (:foreground ,tomorrow-night-red-1))))
   
   ;; ediff
   `(ediff-fine-diff-Ancestor                ((t (:background "#885555"))))
   `(ediff-fine-diff-A                       ((t (:background "#885555"))))
   `(ediff-fine-diff-B                       ((t (:background "#558855"))))
   `(ediff-fine-diff-C                       ((t (:background "#555588"))))
   `(ediff-current-diff-Ancestor             ((t (:background "#663333"))))
   `(ediff-current-diff-A                    ((t (:background "#663333"))))
   `(ediff-current-diff-B                    ((t (:background "#336633"))))
   `(ediff-current-diff-C                    ((t (:background "#333366"))))
   `(ediff-even-diff-Ancestor                ((t (:background "#181a1f"))))
   `(ediff-even-diff-A                       ((t (:background "#181a1f"))))
   `(ediff-even-diff-B                       ((t (:background "#181a1f"))))
   `(ediff-even-diff-C                       ((t (:background "#181a1f"))))
   `(ediff-odd-diff-Ancestor                 ((t (:background "#181a1f"))))
   `(ediff-odd-diff-A                        ((t (:background "#181a1f"))))
   `(ediff-odd-diff-B                        ((t (:background "#181a1f"))))
   `(ediff-odd-diff-C                        ((t (:background "#181a1f"))))

   ;; magit
   `(magit-section-highlight ((t (:background ,tomorrow-night-bg-hl))))
   `(magit-section-heading ((t (:foreground ,tomorrow-night-orange-2 :weight bold))))
   `(magit-section-heading-selection ((t (:foreground ,tomorrow-night-fg :weight bold))))
   `(magit-diff-file-heading ((t (:weight bold))))
   `(magit-diff-file-heading-highlight ((t (:background ,tomorrow-night-gray :weight bold))))
   `(magit-diff-file-heading-selection ((t (:foreground ,tomorrow-night-orange-2 :background ,tomorrow-night-bg-hl :weight bold))))
   `(magit-diff-hunk-heading ((t (:foreground ,tomorrow-night-mono-2 :background ,tomorrow-night-gray))))
   `(magit-diff-hunk-heading-highlight ((t (:foreground ,tomorrow-night-mono-1 :background ,tomorrow-night-mono-3))))
   `(magit-diff-hunk-heading-selection ((t (:foreground ,tomorrow-night-purple :background ,tomorrow-night-mono-3))))
   `(magit-diff-context ((t (:foreground ,tomorrow-night-fg))))
   `(magit-diff-context-highlight ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-fg))))
   `(magit-diffstat-added ((t (:foreground ,tomorrow-night-green))))
   `(magit-diffstat-removed ((t (:foreground ,tomorrow-night-red-1))))
   `(magit-process-ok ((t (:foreground ,tomorrow-night-green))))
   `(magit-process-ng ((t (:foreground ,tomorrow-night-red-1))))
   `(magit-log-author ((t (:foreground ,tomorrow-night-orange-2))))
   `(magit-log-date ((t (:foreground ,tomorrow-night-mono-2))))
   `(magit-log-graph ((t (:foreground ,tomorrow-night-silver))))
   `(magit-sequence-pick ((t (:foreground ,tomorrow-night-orange-2))))
   `(magit-sequence-stop ((t (:foreground ,tomorrow-night-green))))
   `(magit-sequence-part ((t (:foreground ,tomorrow-night-orange-1))))
   `(magit-sequence-head ((t (:foreground ,tomorrow-night-blue))))
   `(magit-sequence-drop ((t (:foreground ,tomorrow-night-red-1))))
   `(magit-sequence-done ((t (:foreground ,tomorrow-night-mono-2))))
   `(magit-sequence-onto ((t (:foreground ,tomorrow-night-mono-2))))
   `(magit-bisect-good ((t (:foreground ,tomorrow-night-green))))
   `(magit-bisect-skip ((t (:foreground ,tomorrow-night-orange-1))))
   `(magit-bisect-bad ((t (:foreground ,tomorrow-night-red-1))))
   `(magit-blame-heading ((t (:background ,tomorrow-night-bg-1 :foreground ,tomorrow-night-mono-2))))
   `(magit-blame-hash ((t (:background ,tomorrow-night-bg-1 :foreground ,tomorrow-night-purple))))
   `(magit-blame-name ((t (:background ,tomorrow-night-bg-1 :foreground ,tomorrow-night-orange-2))))
   `(magit-blame-date ((t (:background ,tomorrow-night-bg-1 :foreground ,tomorrow-night-mono-3))))
   `(magit-blame-summary ((t (:background ,tomorrow-night-bg-1 :foreground ,tomorrow-night-mono-2))))
   `(magit-dimmed ((t (:foreground ,tomorrow-night-mono-2))))
   `(magit-hash ((t (:foreground ,tomorrow-night-purple))))
   `(magit-tag  ((t (:foreground ,tomorrow-night-orange-1 :weight bold))))
   `(magit-branch-remote  ((t (:foreground ,tomorrow-night-green :weight bold))))
   `(magit-branch-local   ((t (:foreground ,tomorrow-night-blue :weight bold))))
   `(magit-branch-current ((t (:foreground ,tomorrow-night-blue :weight bold :box t))))
   `(magit-head           ((t (:foreground ,tomorrow-night-blue :weight bold))))
   `(magit-refname        ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-fg :weight bold))))
   `(magit-refname-stash  ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-fg :weight bold))))
   `(magit-refname-wip    ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-fg :weight bold))))
   `(magit-signature-good      ((t (:foreground ,tomorrow-night-green))))
   `(magit-signature-bad       ((t (:foreground ,tomorrow-night-red-1))))
   `(magit-signature-untrusted ((t (:foreground ,tomorrow-night-orange-1))))
   `(magit-cherry-unmatched    ((t (:foreground ,tomorrow-night-cyan))))
   `(magit-cherry-equivalent   ((t (:foreground ,tomorrow-night-purple))))
   `(magit-reflog-commit       ((t (:foreground ,tomorrow-night-green))))
   `(magit-reflog-amend        ((t (:foreground ,tomorrow-night-purple))))
   `(magit-reflog-merge        ((t (:foreground ,tomorrow-night-green))))
   `(magit-reflog-checkout     ((t (:foreground ,tomorrow-night-blue))))
   `(magit-reflog-reset        ((t (:foreground ,tomorrow-night-red-1))))
   `(magit-reflog-rebase       ((t (:foreground ,tomorrow-night-purple))))
   `(magit-reflog-cherry-pick  ((t (:foreground ,tomorrow-night-green))))
   `(magit-reflog-remote       ((t (:foreground ,tomorrow-night-cyan))))
   `(magit-reflog-other        ((t (:foreground ,tomorrow-night-cyan))))

   ;; message
   `(message-cited-text ((t (:foreground ,tomorrow-night-green))))
   `(message-header-cc ((t (:foreground ,tomorrow-night-orange-1 :weight bold))))
   `(message-header-name ((t (:foreground ,tomorrow-night-purple))))
   `(message-header-newsgroups ((t (:foreground ,tomorrow-night-orange-2 :weight bold :slant italic))))
   `(message-header-other ((t (:foreground ,tomorrow-night-red-1))))
   `(message-header-subject ((t (:foreground ,tomorrow-night-blue))))
   `(message-header-to ((t (:foreground ,tomorrow-night-orange-2 :weight bold))))
   `(message-header-xheader ((t (:foreground ,tomorrow-night-silver))))
   `(message-mml ((t (:foreground ,tomorrow-night-purple))))
   `(message-separator ((t (:foreground ,tomorrow-night-mono-3 :slant italic))))

   ;; epa
   `(epa-field-body ((t (:foreground ,tomorrow-night-blue :slant italic))))
   `(epa-field-name ((t (:foreground ,tomorrow-night-cyan :weight bold))))

   ;; notmuch
   `(notmuch-crypto-decryption ((t (:foreground ,tomorrow-night-purple :background ,tomorrow-night-black))))
   `(notmuch-crypto-signature-bad ((t (:foreground ,tomorrow-night-red-1 :background ,tomorrow-night-black))))
   `(notmuch-crypto-signature-good ((t (:foreground ,tomorrow-night-green :background ,tomorrow-night-black))))
   `(notmuch-crypto-signature-good-key ((t (:foreground ,tomorrow-night-green :background ,tomorrow-night-black))))
   `(notmuch-crypto-signature-unknown ((t (:foreground ,tomorrow-night-orange-1 :background ,tomorrow-night-black))))
   `(notmuch-hello-logo-background ((t (:inherit default))))
   `(notmuch-message-summary-face ((t (:background ,tomorrow-night-black))))
   `(notmuch-search-count ((t (:inherit default :foreground ,tomorrow-night-silver))))
   `(notmuch-search-date ((t (:inherit default :foreground ,tomorrow-night-purple))))
   `(notmuch-search-matching-authors ((t (:inherit default :foreground ,tomorrow-night-orange-2))))
   `(notmuch-search-non-matching-authors ((t (:inherit font-lock-comment-face :slant italic))))
   `(notmuch-tag-added ((t (:underline t))))
   `(notmuch-tag-deleted ((t (:strike-through ,tomorrow-night-red-2))))
   `(notmuch-tag-face ((t (:foreground ,tomorrow-night-green))))
   `(notmuch-tag-unread ((t (:foreground ,tomorrow-night-red-1))))
   `(notmuch-tree-match-author-face ((t (:inherit notmuch-search-matching-authors))))
   `(notmuch-tree-match-date-face ((t (:inherit notmuch-search-date))))
   `(notmuch-tree-match-face ((t (:weight semi-bold))))
   `(notmuch-tree-match-tag-face ((t (:inherit notmuch-tag-face))))
   `(notmuch-tree-no-match-face ((t (:slant italic :weight light :inherit font-lock-comment-face))))

   ;; elfeed
   `(elfeed-log-debug-level-face ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-green))))
   `(elfeed-log-error-level-face ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-red-1))))
   `(elfeed-log-info-level-face ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-blue))))
   `(elfeed-log-warn-level-face ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-orange-1))))
   `(elfeed-search-date-face ((t (:foreground ,tomorrow-night-purple))))
   `(elfeed-search-feed-face ((t (:foreground ,tomorrow-night-orange-2))))
   `(elfeed-search-tag-face ((t (:foreground ,tomorrow-night-green))))
   `(elfeed-search-title-face ((t (:foreground ,tomorrow-night-mono-1))))
   `(elfeed-search-unread-count-face ((t (:foreground ,tomorrow-night-silver))))

   ;; perspective
   `(persp-selected-face ((t (:foreground ,tomorrow-night-blue))))

   ;; powerline
   `(powerline-active1 ((,class (:background ,tomorrow-night-bg-hl :foreground ,tomorrow-night-purple))))
   `(powerline-active2 ((,class (:background ,tomorrow-night-bg-hl :foreground ,tomorrow-night-purple))))
   `(powerline-inactive1 ((,class (:background ,tomorrow-night-bg :foreground ,tomorrow-night-fg))))
   `(powerline-inactive2 ((,class (:background ,tomorrow-night-bg :foreground ,tomorrow-night-fg))))

   ;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((t (:foreground ,tomorrow-night-blue))))
   `(rainbow-delimiters-depth-2-face ((t (:foreground ,tomorrow-night-green))))
   `(rainbow-delimiters-depth-3-face ((t (:foreground ,tomorrow-night-orange-1))))
   `(rainbow-delimiters-depth-4-face ((t (:foreground ,tomorrow-night-cyan))))
   `(rainbow-delimiters-depth-5-face ((t (:foreground ,tomorrow-night-purple))))
   `(rainbow-delimiters-depth-6-face ((t (:foreground ,tomorrow-night-orange-2))))
   `(rainbow-delimiters-depth-7-face ((t (:foreground ,tomorrow-night-blue))))
   `(rainbow-delimiters-depth-8-face ((t (:foreground ,tomorrow-night-green))))
   `(rainbow-delimiters-depth-9-face ((t (:foreground ,tomorrow-night-orange-1))))
   `(rainbow-delimiters-depth-10-face ((t (:foreground ,tomorrow-night-cyan))))
   `(rainbow-delimiters-depth-11-face ((t (:foreground ,tomorrow-night-purple))))
   `(rainbow-delimiters-depth-12-face ((t (:foreground ,tomorrow-night-orange-2))))
   `(rainbow-delimiters-unmatched-face ((t (:foreground ,tomorrow-night-red-1 :weight bold))))

   ;; rbenv
   `(rbenv-active-ruby-face ((t (:foreground ,tomorrow-night-green))))

   ;; elixir
   `(elixir-atom-face ((t (:foreground ,tomorrow-night-cyan))))
   `(elixir-attribute-face ((t (:foreground ,tomorrow-night-red-1))))

   ;; show-paren
   `(show-paren-match ((,class (:foreground ,tomorrow-night-purple :inherit bold :underline t))))
   `(show-paren-mismatch ((,class (:foreground ,tomorrow-night-red-1 :inherit bold :underline t))))

   ;; sh-mode
   `(sh-heredoc ((t (:inherit font-lock-string-face :slant italic))))

   ;; cider
   `(cider-fringe-good-face ((t (:foreground ,tomorrow-night-green))))

   ;; sly
   `(sly-error-face ((t (:underline (:color ,tomorrow-night-red-1 :style wave)))))
   `(sly-mrepl-note-face ((t (:inherit font-lock-comment-face))))
   `(sly-mrepl-output-face ((t (:inherit font-lock-string-face))))
   `(sly-mrepl-prompt-face ((t (:inherit comint-highlight-prompt))))
   `(sly-note-face ((t (:underline (:color ,tomorrow-night-green :style wave)))))
   `(sly-style-warning-face ((t (:underline (:color ,tomorrow-night-orange-2 :style wave)))))
   `(sly-warning-face ((t (:underline (:color ,tomorrow-night-orange-1 :style wave)))))

   ;; smartparens
   `(sp-show-pair-mismatch-face ((t (:foreground ,tomorrow-night-red-1 :background ,tomorrow-night-gray :weight bold))))
   `(sp-show-pair-match-face ((t (:background ,tomorrow-night-gray :weight bold))))

   ;; lispy
   `(lispy-face-hint ((t (:background ,tomorrow-night-border :foreground ,tomorrow-night-orange-2))))

   ;; lispyville
   `(lispyville-special-face ((t (:foreground ,tomorrow-night-red-1))))

   ;; spaceline
   `(spaceline-flycheck-error  ((,class (:foreground ,tomorrow-night-red-1))))
   `(spaceline-flycheck-info   ((,class (:foreground ,tomorrow-night-green))))
   `(spaceline-flycheck-warning((,class (:foreground ,tomorrow-night-orange-1))))
   `(spaceline-python-venv ((,class (:foreground ,tomorrow-night-purple))))

   ;; solaire mode
   `(solaire-default-face ((,class (:inherit default :background ,tomorrow-night-black))))
   `(solaire-minibuffer-face ((,class (:inherit default :background ,tomorrow-night-black))))

   ;; web-mode
   `(web-mode-doctype-face ((t (:inherit font-lock-comment-face))))
   `(web-mode-error-face ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-red-1))))
   `(web-mode-html-attr-equal-face ((t (:inherit default))))
   `(web-mode-html-attr-name-face ((t (:foreground ,tomorrow-night-orange-1))))
   `(web-mode-html-tag-bracket-face ((t (:inherit default))))
   `(web-mode-html-tag-face ((t (:foreground ,tomorrow-night-red-1))))
   `(web-mode-symbol-face ((t (:foreground ,tomorrow-night-orange-1))))

   ;; nxml
   `(nxml-attribute-local-name ((t (:foreground ,tomorrow-night-orange-1))))
   `(nxml-element-local-name ((t (:foreground ,tomorrow-night-red-1))))
   `(nxml-markup-declaration-delimiter ((t (:inherit (font-lock-comment-face nxml-delimiter)))))
   `(nxml-processing-instruction-delimiter ((t (:inherit nxml-markup-declaration-delimiter))))

   ;; flx-ido
   `(flx-highlight-face ((t (:inherit (link) :weight bold))))

   ;; rpm-spec-mode
   `(rpm-spec-tag-face ((t (:foreground ,tomorrow-night-blue))))
   `(rpm-spec-obsolete-tag-face ((t (:foreground "#FFFFFF" :background ,tomorrow-night-red-2))))
   `(rpm-spec-macro-face ((t (:foreground ,tomorrow-night-orange-2))))
   `(rpm-spec-var-face ((t (:foreground ,tomorrow-night-red-1))))
   `(rpm-spec-doc-face ((t (:foreground ,tomorrow-night-purple))))
   `(rpm-spec-dir-face ((t (:foreground ,tomorrow-night-cyan))))
   `(rpm-spec-package-face ((t (:foreground ,tomorrow-night-red-2))))
   `(rpm-spec-ghost-face ((t (:foreground ,tomorrow-night-red-2))))
   `(rpm-spec-section-face ((t (:foreground ,tomorrow-night-orange-2))))

   ;; guix
   `(guix-true ((t (:foreground ,tomorrow-night-green :weight bold))))
   `(guix-build-log-phase-end ((t (:inherit success))))
   `(guix-build-log-phase-start ((t (:inherit success :weight bold))))

   ;; gomoku
   `(gomoku-O ((t (:foreground ,tomorrow-night-red-1 :weight bold))))
   `(gomoku-X ((t (:foreground ,tomorrow-night-green :weight bold))))

   ;; tabbar
   `(tabbar-default ((,class (:foreground ,tomorrow-night-fg :background ,tomorrow-night-black))))
   `(tabbar-highlight ((,class (:underline t))))
   `(tabbar-button ((,class (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg))))
   `(tabbar-button-highlight ((,class (:inherit 'tabbar-button :inverse-video t))))
   `(tabbar-modified ((,class (:inherit tabbar-button :foreground ,tomorrow-night-purple :weight light :slant italic))))
   `(tabbar-unselected ((,class (:inherit tabbar-default :foreground ,tomorrow-night-fg :background ,tomorrow-night-black :slant italic :underline nil :box (:line-width 1 :color ,tomorrow-night-bg)))))
   `(tabbar-unselected-modified ((,class (:inherit tabbar-modified :background ,tomorrow-night-black :underline nil :box (:line-width 1 :color ,tomorrow-night-bg)))))
   `(tabbar-selected ((,class (:inherit tabbar-default :foreground ,tomorrow-night-fg :background ,tomorrow-night-bg :weight bold :underline nil :box (:line-width 1 :color ,tomorrow-night-bg)))))
   `(tabbar-selected-modified ((,class (:inherit tabbar-selected :foreground ,tomorrow-night-purple :underline nil :box (:line-width 1 :color ,tomorrow-night-bg)))))

   ;; linum
   `(linum ((t (:foreground ,tomorrow-night-gutter :background ,tomorrow-night-bg))))
   ;; hlinum
   `(linum-highlight-face ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg))))
   ;; native line numbers (emacs version >=26)
   `(line-number ((t (:foreground ,tomorrow-night-gutter :background ,tomorrow-night-bg))))
   `(line-number-current-line ((t (:foreground ,tomorrow-night-fg :background ,tomorrow-night-bg))))

   ;; regexp-builder
   `(reb-match-0 ((t (:background ,tomorrow-night-gray))))
   `(reb-match-1 ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-purple :weight semi-bold))))
   `(reb-match-2 ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-green :weight semi-bold))))
   `(reb-match-3 ((t (:background ,tomorrow-night-black :foreground ,tomorrow-night-orange-2 :weight semi-bold))))

   ;; desktop-entry
   `(desktop-entry-deprecated-keyword-face ((t (:inherit font-lock-warning-face))))
   `(desktop-entry-group-header-face ((t (:inherit font-lock-type-face))))
   `(desktop-entry-locale-face ((t (:inherit font-lock-string-face))))
   `(desktop-entry-unknown-keyword-face ((t (:underline (:color ,tomorrow-night-red-1 :style wave) :inherit font-lock-keyword-face))))
   `(desktop-entry-value-face ((t (:inherit default))))

   ;; latex-mode
   `(font-latex-sectioning-0-face ((t (:foreground ,tomorrow-night-blue :height 1.0))))
   `(font-latex-sectioning-1-face ((t (:foreground ,tomorrow-night-blue :height 1.0))))
   `(font-latex-sectioning-2-face ((t (:foreground ,tomorrow-night-blue :height 1.0))))
   `(font-latex-sectioning-3-face ((t (:foreground ,tomorrow-night-blue :height 1.0))))
   `(font-latex-sectioning-4-face ((t (:foreground ,tomorrow-night-blue :height 1.0))))
   `(font-latex-sectioning-5-face ((t (:foreground ,tomorrow-night-blue :height 1.0))))
   `(font-latex-bold-face ((t (:foreground ,tomorrow-night-green :weight bold))))
   `(font-latex-italic-face ((t (:foreground ,tomorrow-night-green))))
   `(font-latex-warning-face ((t (:foreground ,tomorrow-night-red-1))))
   `(font-latex-doctex-preprocessor-face ((t (:foreground ,tomorrow-night-cyan))))
   `(font-latex-script-char-face ((t (:foreground ,tomorrow-night-mono-2))))

   ;; org-mode
   `(org-date ((t (:foreground ,tomorrow-night-cyan))))
   `(org-ellipsis ((t (:underline nil))))
   `(org-document-info ((t (:foreground ,tomorrow-night-mono-2))))
   `(org-document-info-keyword ((t (:inherit org-meta-line :underline nil))))
   `(org-document-title ((t (:weight bold))))
   ;; `(org-block-begin-line ((t (:background ,tomorrow-night-border))))
   `(org-block ((t (:background ,tomorrow-night-gutter))))
   ;; `(org-block-end-line ((t (:background ,tomorrow-night-border))))
   `(org-footnote ((t (:foreground ,tomorrow-night-cyan))))
   `(org-sexp-date ((t (:foreground ,tomorrow-night-cyan))))

   ;; calendar
   `(diary ((t (:inherit warning))))
   `(holiday ((t (:foreground ,tomorrow-night-green))))

   ;; gud
   `(breakpoint-disabled ((t (:foreground ,tomorrow-night-orange-1))))
   `(breakpoint-enabled ((t (:foreground ,tomorrow-night-red-1 :weight bold))))

   ;; realgud
   `(realgud-overlay-arrow1        ((t (:foreground ,tomorrow-night-green))))
   `(realgud-overlay-arrow3        ((t (:foreground ,tomorrow-night-orange-1))   `(realgud-overlay-arrow2        ((t (:foreground ,tomorrow-night-orange-2))))
                                    ))
   '(realgud-bp-enabled-face       ((t (:inherit (error)))))
   `(realgud-bp-disabled-face      ((t (:inherit (secondary-selection)))))
   `(realgud-bp-line-enabled-face  ((t (:box (:color ,tomorrow-night-red-1)))))
   `(realgud-bp-line-disabled-face ((t (:box (:color ,tomorrow-night-gray)))))
   `(realgud-line-number           ((t (:foreground ,tomorrow-night-mono-2))))
   `(realgud-backtrace-number      ((t (:inherit (secondary-selection)))))

   ;; rmsbolt
   `(rmsbolt-current-line-face ((t (:inherit hl-line :weight bold))))

   ;; ruler-mode
   `(ruler-mode-column-number ((t (:inherit ruler-mode-default))))
   `(ruler-mode-comment-column ((t (:foreground ,tomorrow-night-red-1))))
   `(ruler-mode-current-column ((t (:foreground ,tomorrow-night-accent :inherit ruler-mode-default))))
   `(ruler-mode-default ((t (:inherit mode-line))))
   `(ruler-mode-fill-column ((t (:foreground ,tomorrow-night-orange-1 :inherit ruler-mode-default))))
   `(ruler-mode-fringes ((t (:foreground ,tomorrow-night-green :inherit ruler-mode-default))))
   `(ruler-mode-goal-column ((t (:foreground ,tomorrow-night-cyan :inherit ruler-mode-default))))
   `(ruler-mode-margins ((t (:inherit ruler-mode-default))))
   `(ruler-mode-tab-stop ((t (:foreground ,tomorrow-night-mono-3 :inherit ruler-mode-default))))

   ;; undo-tree
   `(undo-tree-visualizer-current-face ((t (:foreground ,tomorrow-night-red-1))))
   `(undo-tree-visualizer-register-face ((t (:foreground ,tomorrow-night-orange-1))))
   `(undo-tree-visualizer-unmodified-face ((t (:foreground ,tomorrow-night-cyan))))

   ;; tab-bar-mode
   `(tab-bar-tab-inactive ((t (:background ,tomorrow-night-bg-hl :foreground ,tomorrow-night-fg))))
   `(tab-bar-tab          ((t (:background ,tomorrow-night-bg :foreground ,tomorrow-night-purple))))
   `(tab-bar              ((t (:background ,tomorrow-night-bg-hl))))
   ))

(tomorrow-night-with-color-variables
  (custom-theme-set-variables
   'tomorrow-night
   ;; fill-column-indicator
   `(fci-rule-color ,tomorrow-night-gray)

   ;; tetris
   ;; | Tetromino | Color                    |
   ;; | O         | `tomorrow-night-orange-2' |
   ;; | J         | `tomorrow-night-blue'     |
   ;; | L         | `tomorrow-night-orange-1' |
   ;; | Z         | `tomorrow-night-red-1'    |
   ;; | S         | `tomorrow-night-green'    |
   ;; | T         | `tomorrow-night-purple'   |
   ;; | I         | `tomorrow-night-cyan'     |
   '(tetris-x-colors
     [[229 192 123] [97 175 239] [209 154 102] [224 108 117] [152 195 121] [198 120 221] [86 182 194]])

   ;; ansi-color
   `(ansi-color-names-vector
     [,tomorrow-night-black ,tomorrow-night-red-1 ,tomorrow-night-green ,tomorrow-night-orange-2
      ,tomorrow-night-blue ,tomorrow-night-purple ,tomorrow-night-cyan ,tomorrow-night-fg])
   ))

(defvar tomorrow-night-theme-force-faces-for-mode t
  "If t, tomorrow-night-theme will use Face Remapping to alter the theme faces for
the current buffer based on its mode in an attempt to mimick the Tomorrow Night
Theme from Atom.io as best as possible.
The reason this is required is because some modes (html-mode, jyaml-mode, ...)
do not provide the necessary faces to do theming without conflicting with other
modes.
Current modes, and their faces, impacted by this variable:
* js2-mode: font-lock-constant-face, font-lock-doc-face, font-lock-variable-name-face
* html-mode: font-lock-function-name-face, font-lock-variable-name-face
")

;; Many modes in Emacs do not define their own faces and instead use standard Emacs faces when it comes to theming.
;; That being said, to have a real "Tomorrow Night Theme" for Emacs, we need to work around this so that these themes look
;; as much like "Tomorrow Night Theme" as possible.  This means using per-buffer faces via "Face Remapping":
;;
;;   http://www.gnu.org/software/emacs/manual/html_node/elisp/Face-Remapping.html
;;
;; Of course, this might be confusing to some when in one mode they see keywords highlighted in one face and in another
;; mode they see a different face.  That being said, you can set the `tomorrow-night-theme-force-faces-for-mode` variable to
;; `nil` to disable this feature.
(defun tomorrow-night-theme-change-faces-for-mode ()
  (interactive)
  (when (or tomorrow-night-theme-force-faces-for-mode (called-interactively-p))
    (tomorrow-night-with-color-variables
     (cond
      ((member major-mode '(js2-mode))
       (face-remap-add-relative 'font-lock-constant-face :foreground tomorrow-night-orange-1)
       (face-remap-add-relative 'font-lock-doc-face '(:inherit (font-lock-comment-face)))
       (face-remap-add-relative 'font-lock-variable-name-face :foreground tomorrow-night-mono-1))
      ((member major-mode '(html-mode))
       (face-remap-add-relative 'font-lock-function-name-face :foreground tomorrow-night-red-1)
       (face-remap-add-relative 'font-lock-variable-name-face :foreground tomorrow-night-orange-1))))))

(add-hook 'after-change-major-mode-hook 'tomorrow-night-theme-change-faces-for-mode)

;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))
;; Automatically add this theme to the load path

(provide-theme 'tomorrow-night)

;; Local Variables:
;; no-byte-compile: t
;; End:
;;; tomorrow-night-theme.el ends here
