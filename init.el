3;; init.el -*- lexical-binding: t; -*-

;;-----------------------------------------------------------------------;;
;;                           TERROR EMACS                                ;;
;;         A simple Emacs setup for the terror of academic life          ;;
;;                                                                       ;;
;; This program is free software: you can redistribute it and/or modify  ;;
;; it under the terms of the GNU General Public License as published by  ;;
;; the Free Software Foundation, either version 3 of the License, or     ;;
;; any later version.                                                    ;;
;;                                                                       ;;
;; This program is distributed in the hope that it will be useful,       ;;
;; but WITHOUT ANY WARRANTY; without even the implied warranty of        ;;
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         ;;
;; GNU General Public License for more details.                          ;;
;;                                                                       ;;
;; You should have received a copy of the GNU General Public License     ;;
;; along with this program.  If not, see <http://www.gnu.org/licenses/>. ;;
;;-----------------------------------------------------------------------;;

(setq gc-cons-threshold (* 50 1000 1000))

(setq user-full-name "DELM"
      user-mail-address "dlmayhem@riseup.net")

(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(set-fringe-mode 4)
(global-visual-line-mode 1)
(global-auto-revert-mode 1)
(if (window-system)
    (global-hl-line-mode 1))
(set-face-font 'default "JetBrainsMono 9")
(set-face-font 'variable-pitch "JetBrainsMono 9")
(set-fontset-font "fontset-default" 'greek
		  (font-spec :family "Gentium" :size 15))
(fset 'yes-or-no-p 'y-or-n-p)
(delete-selection-mode 1)
(column-number-mode)
(put 'downcase-region 'disabled nil)
(set-frame-parameter (selected-frame) 'internal-border-width 0)
(dolist (mode '(c-mode-hook
		scheme-mode-hook
                emacs-lisp-mode-hook
                sh-mode-hook
                python-mode-hook
                LaTeX-mode-hook
		web-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode t))))
(setq-default cursor-in-non-selected-windows nil
              frame-title-format '("%f [%m]"))
(setq default-fill-column 100
      make-backup-files nil
      inhibit-startup-message t
      use-dialog-box nil
      vc-follow-symlinks t
      byte-compile-warnings '(cl-functions)
      tramp-default-method "ssh"
      custom-file "~/.emacs.d/custom.el"
      custom-theme-directory "~/.emacs.d/themes"
      global-auto-revert-non-file-buffers t
      message-kill-buffer-on-exit t
      large-file-warning-threshold nil)
(load custom-file)

(require 'package)

 (setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package vertico
  :ensure t
  :init
  (vertico-mode))

(use-package vertico-directory
  :after vertico
  :ensure nil
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(use-package savehist
  :init
  (savehist-mode))

(use-package marginalia
  :after vertico
  :ensure t
  :init
  (marginalia-mode)
  :custom
  (marginalia-align 'right))

(use-package consult
    :ensure t
    :demand t
    :bind (("C-s" . consult-line)
           ("C-M-l" . consult-imenu)
           ("C-x b" . consult-buffer)
           :map minibuffer-local-map
           ("C-r" . consult-history)))

(use-package embark
  :ensure t
  :bind (("C-." . embark-act)
         ("C-;" . embark-dwim)
         ("C-h B" . embark-bindings))
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  :config
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :ensure t
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package orderless
  :ensure t
  :after vertico
  :custom
  (completion-styles '(orderless basic)))

(use-package corfu
  :custom
  (corfu-auto t)
  (corfu-quit-no-match t)
  (corfu-separator ?\s)
  :config 
  (global-corfu-mode 1))

(use-package corfu-terminal
  :config
  (unless (display-graphic-p)
    (corfu-terminal-mode +1)))

(use-package doom-themes)

(if window-system
    (load-theme 'td-wilmersdorf t))

(use-package minions
  :init
  (minions-mode))

(use-package nerd-icons
  :if window-system
  :custom
  (nerd-icons-font-family "Symbols Nerd Font Mono"))

(use-package nerd-icons-dired
  :if window-system
  :hook
  (dired-mode . nerd-icons-dired-mode))

(use-package nerd-icons-completion
  :if window-system
  :after marginalia
  :config
  (nerd-icons-completion-mode)
  (add-hook 'marginalia-mode-hook #'nerd-icons-completion-marginalia-setup))

(use-package which-key
  :defer 0
  :config
  (which-key-mode)
  (setq which-key-idle-delay 0.3))

(use-package gnus
  :bind
  ("C-c g" . gnus))

(use-package magit
  :bind ("C-x g" . magit-status))

(use-package smartparens
  :hook (prog-mode . smartparens-mode))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package paren
  :config
  (show-paren-mode 1))

(use-package rainbow-mode
  :defer t
  :hook (org-mode
         emacs-lisp-mode
         web-mode
         c-mode))

(use-package writeroom-mode 
  :bind ("C-c d" . writeroom-mode)
  :config
  (advice-add 'text-scale-adjust :after
              #'visual-fill-column-adjust))

(use-package yasnippet
  :defer t
  :config
  (setq yas-snippet-dirs '("~/.emacs.d/templates/yasnippet"))
  (yas-global-mode 1))

(use-package lorem-ipsum
  :bind (("C-c M-p" . lorem-ipsum-insert-paragraphs)
	 ("C-c M-s" . lorem-ipsum-insert-sentences)))

(use-package pdf-tools
  :defer t
  :custom
  (pdf-view-midnight-colors '("#c6c6c6" . "#1f2024")))

(pdf-loader-install)

(use-package pdf-view-restore
  :after pdf-tools
  :config
  (add-hook 'pdf-view-mode-hook 'pdf-view-restore-mode))

(use-package nov
  :defer t
  :mode ("\\.epub\\'" . nov-mode))

(use-package citar
  :defer t
  :bind (("C-c b" . citar-insert-citation)
	 ("M-p" . citar-org-update-prefix-suffix)
         :map minibuffer-local-map
         ("M-b" . citar-insert-preset))
  :custom
  (citar-bibliography '("~/Documentos/refs.bib"))
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))

(use-package citar-embark
  :after citar embark
  :no-require
  ;; :init
  ;; (setq citar-at-point-function 'embark-act)
  :config (citar-embark-mode))

(use-package citar-org-roam
  :after (citar org-roam)
  :config (citar-org-roam-mode))

(use-package ebib
  :defer t
  :config
  (require 'ebib-biblio)
  (define-key ebib-index-mode-map (kbd "B") #'ebib-biblio-import-doi)
  (define-key biblio-selection-mode-map (kbd "e") #'ebib-biblio-selection-import))

(use-package telega
  :ensure t
  :defer t
  :config
  (setq telega-use-images nil))

(use-package elfeed
  :no-require t
  :bind ("C-x w" . elfeed))

(use-package elfeed-org
  :ensure t
  :after elfeed 
  :config
  (elfeed-org)
  (setq rmh-elfeed-org-files (list "~/.emacs.d/elfeed.org")))

(use-package arxiv-mode
  :ensure t
  :bind ("C-x x" . arxiv-search)
  :config
  (setq arxiv-default-download-folder "~/Documentos/"
	arxiv-pop-up-new-frame nil))

(use-package org
  :commands (org-capture org-agenda)
  :bind (("C-c l" . org-store-link)
         ("C-c a" . org-agenda)
         ("C-c c" . org-capture))
  :hook (org-mode . smartparens-mode)
  :custom
  (org-export-backends
   '(beamer html icalendar latex md odt))
  (org-capture-templates
   '(("b" "Birthday" entry
      (file+headline "~/Proyectos/org/agenda.org" "Birthdays")
      "** BIRTHDAY %^{Name}\n%<<%Y-%^{Month number}-%^{Day number} ++1y>>%?"
      :empty-lines-before 1 :empty-lines-after 1
      :immediate-finish t)
     ("d" "Deadline" entry
      (file+headline "~/Proyectos/org/agenda.org" "Deadlines")
      "** TODO %^{Task}\nDEADLINE %^T%?"
      :empty-lines-before 1 )
     ("e" "Event")
     ("ea" "Academic event" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** %^{|CLASS|CONGRESS|SEMINAR|SYMPOSIUM} %^{Name}\n- SCHEDULED %^t--%^t\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("ec" "Craic" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** PARTY %^{Name}\n- SCHEDULED %^T\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("ef" "Festival" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** FEST %^{Name}\n- SCHEDULED %^t--%^t\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("eg" "Gig" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** CONCERT %^{Band}\n- SCHEDULED %^T\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("em" "Meeting" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** MEETING %^{Meeting}\n- SCHEDULED %^T--%^T\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("ep" "Practice" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** PRACTICE %^{What?}\n- SCHEDULED %^T\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("et" "Talk" entry
      (file+headline "~/Proyectos/org/agenda.org" "Events")
      "** TALK %^{Talk's name}\n- Author :: %^{Name}\n- SCHEDULED %^T\n- Location :: %^{Place/link}%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("j" "Task" entry
      (file+headline "~/Proyectos/org/agenda.org" "Frequent Tasks")
      "** %^{Task}\n %^T%?"
      :empty-lines-before 1 :empty-lines-after 1)
     ("l" "Loans" entry
      (file+headline "~/.agenda.org" "On loan")
      "** ON LOAN %^{|BOOK|MUSIC|OTHER}\n- Title :: %^{Title}\n- To whom :: %^{Name}\n- Date :: %t%?"
      :empty-lines-before 1 :empty-lines-after 1
      :immediate-finish t)
     ("t" "To-Do" entry
      (file+headline "~/Proyectos/org/agenda.org" "To-Do")
      "** TODO %^{To do}\n%?"
      :empty-lines-before 1 :empty-lines-after 1)))
  (org-todo-keywords
   '((sequence "TODO(t)" "DONE(d)" "ON LOAN(l)")))
  :config
  (setq org-agenda-files '("~/Proyectos/org/agenda.org")
        org-ellipsis " ▾"
        org-footnote-auto-adjust t
        org-fontify-quote-and-verse-blocks t
        org-log-done 'time
        org-capture-bookmark nil
        org-html-validation-link nil
        org-startup-indented t
        org-startup-folded nil
	org-confirm-babel-evaluate nil
	org-image-actual-width 550
        org-format-latex-options (plist-put org-format-latex-options :scale 1.5)))

;; (use-package oc
;;   :defer t
;;   :init
;;   (require 'oc-csl)
;;   (require 'oc-biblatex)
;;   (require 'oc-natbib)
;;   (setq org-cite-global-bibliography '("~/Documentos/refs.bib")))

(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((C . t)
     (scheme . t)
     (python . t)
     (shell . t)
     (dot . t)
     (latex . t)
     (latex-as-png . t)))
  (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images))

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (file-truename "~/Proyectos/org/roam"))
  (org-roam-capture-templates
   '(("a" "Author" plain
      (file "~/.emacs.d/templates/roam/author.org")
      :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)
     ("b" "Bibliography" plain
      (file "~/.emacs.d/templates/roam/biblio.org")
      :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)
     ("c" "Concept" plain
      (file "~/.emacs.d/templates/roam/concept.org")
      :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)
     ("d" "Default" plain
       "%?"
       :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org" "#+title: ${title}\n")
       :unnarrowed t)
     ("f" "Field" plain
      (file "~/.emacs.d/templates/roam/field.org")
      :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)
     ("q" "Quote" plain
      (file "~/.emacs.d/templates/roam/quote.org")
      :target (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                         "#+title: ${title}\n")
      :unnarrowed t)))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n g" . org-roam-ui-mode)
         ("C-c n c" . org-roam-capture)
         ("C-c n j" . org-roam-dailies-capture-today))
  :config
  (setq org-roam-node-display-template
        (concat "${title:*} " (propertize "${tags:10}" 'face 'org-tag)))
  (org-roam-setup)
  (require 'org-roam-protocol))

(use-package org-roam-ui
  :after org-roam
  :config
  (setq org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start nil))

(use-package org-contrib
  :after org
  :config
  (require 'ox-extra)
  (ox-extras-activate '(latex-header-blocks ignore-headlines)))

(use-package ox-twbs
  :after org)

(use-package ob-latex-as-png
  :after org)

(use-package org-bullets
  :no-require t
  :custom
  (org-bullets-bullet-list '("◉" "●" "○" "●" "○" "●")))

(use-package hide-lines)

(use-package hide-mode-line
  :defer t)

(defun terror/slide-setup ()
  (global-hl-line-mode -1)
  (setq org-hide-emphasis-markers t)
  (org-bullets-mode 1)
  (setq text-scale-mode-amount 2)
  (text-scale-mode 1)
  (set-frame-parameter (selected-frame)
                       'internal-border-width 50)
  (org-display-inline-images)
  (toggle-frame-fullscreen)
  (hide-mode-line-mode 1)
  (hide-lines-matching "#\\+begin")
  (hide-lines-matching "#\\+end"))

(defun terror/slide-end ()
  (global-hl-line-mode 1)
  (setq org-hide-emphasis-markers nil)
  (org-bullets-mode -1)
  (setq text-scale-mode-amount 0)
  (text-scale-mode -1)
  (set-frame-parameter (selected-frame)
                       'internal-border-width 0)
  (toggle-frame-fullscreen)
  (hide-mode-line-mode -1)
  (hide-lines-show-all)
  (org-fold-show-all))

(use-package org-tree-slide
  :after org
  :bind ("C-c p" . org-tree-slide-mode)
  :hook ((org-tree-slide-play . terror/slide-setup)
         (org-tree-slide-stop . terror/slide-end))
  :config
  (setq org-tree-slide-slide-in-effect nil
        org-image-actual-width nil
        org-tree-slide-header t
        org-tree-slide-breadcrumbs " > "
        org-tree-slide-activate-message "Let's begin..."
        org-tree-slide-deactivate-message "The end :)"))

(use-package latex
  :ensure auctex
  :defer t
  :init
  (server-force-delete)
  :custom
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-start-server t)
  :config
  (setq TeX-view-program-selection '((output-pdf "PDF Tools")))
  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer))

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode)))

(use-package lilypond-mode
  :defer t
  :load-path "var/lisp/lilypond-mode"
  :config
  (setq auto-mode-alist
        (cons '("\\.ly$" . LilyPond-mode) auto-mode-alist))
  (add-hook 'LilyPond-mode-hook (lambda () (turn-on-font-lock)))
  (setq LilyPond-command-alist '(("LilyPond" "lilypond %s" "%s" "%l" "View")
                                 ("2PS" "lilypond -f ps %s" "%s" "%p" "ViewPS")
                                 ("Book" "lilypond-book %x" "%x" "%l" "LaTeX")
                                 ("LaTeX" "latex '\\nonstopmode\\input %l'" "%l" "%d" "ViewDVI")
                                 ("View" "open %f")
                                 ("ViewPDF" "open %f")
                                 ("ViewPS" "gv --watch %p")
                                 ("Midi" "")
                                 ("MidiAll" ""))))

(use-package extempore-mode
  :defer t)

(use-package eglot
  :ensure t
  :defer t
  :hook ((python-mode . eglot-ensure)
	 (c-mode . eglot-ensure)))

(use-package tree-sitter
  :ensure t
  :defer t
  :hook ((python-mode . tree-sitter-mode)
	 (python-mode . tree-sitter-hl-mode)
	 (c-mode . tree-sitter-mode)
	 (c-mode . tree-sitter-hl-mode)
	 (sh-mode . tree-sitter-mode)
	 (sh-mode . tree-sitter-hl-mode)))

(use-package tree-sitter-langs
  :ensure t
  :defer t
  :after tree-sitter)

(use-package pyvenv
  :ensure t
  :defer t
  :config
  (pyvenv-mode t))

(use-package pyvenv-auto
  :hook ((python-mode . pyvenv-auto-run)))

(use-package blacken
  :ensure t
  :hook (python-mode . blacken-mode)
  :bind (:map python-mode-map
              ("C-c f" . blacken-buffer)))

(use-package geiser-guile
  :ensure t
  :defer t)

(use-package emmet-mode
  :ensure t
  :defer t
  :hook ((web-mode . emmet-mode)
	 (web-mode . yas-global-mode))
  :config
  (setq emmet-indent-after-insert nil
        emmet-indentation 2))

(use-package web-mode
  :ensure t
  :defer t
  :mode "\\.html?\\'" 
  :mode "\\.css\\'"
  :mode "\\.phtml\\'"
  :mode "\\.tpl\\.php\\'"
  :mode "\\.[agj]sp\\'"
  :mode "\\.as[cp]x\\'"
  :mode "\\.erb\\'"
  :mode "\\.mustache\\'"
  :mode "\\.djhtml\\'"
  :config
  (setq-default web-mode-code-indent-offset 2
		web-mode-markup-indent-offset 2
		web-mode-attribute-indent-offset 2))

(use-package simple-httpd
  :ensure t)

(use-package impatient-mode
  :ensure t)

(use-package htmlize
  :ensure t)

(use-package impatient-mode
  :ensure t)

(use-package dired
  :ensure nil
  :defer 1
  :config
  (setq dired-recursive-copies 'always
        dired-listing-switches "--group-directories-first -alh"))
	;; dired-omit-files "^\\.[^.].*"))

(use-package dired-subtree
  :ensure t
  :after dired
  :config
  (define-key dired-mode-map (kbd "<tab>") 'dired-subtree-toggle))

(use-package dired-hide-dotfiles
  :after dired
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :config
  (define-key dired-mode-map "." #'dired-hide-dotfiles-mode))

(use-package dired-open
  :after dired
  :config
  (setq dired-open-extensions '(("mp3" . "mpv")
                                ("mp4" . "mpv")
                                ("mkv" . "mpv")
                                ("png" . "nsxiv -b")
                                ("gif" . "nsxiv -b")
                                ("jpg" . "nsxiv -b")
                                ("jpeg" . "nsxiv -b"))))

;; (add-hook 'emacs-startup-hook
;;           (lambda ()
;;             (message "Emacs ready in %s with %d garbage collections."
;;                      (format "%.2f seconds"
;;                              (float-time
;;                               (time-subtract after-init-time before-init-time)))
;;                      gcs-done)))

(setq gc-cons-threshold (* 2 1000 1000))

;;; Happy hacking! ;;;
